---
title: بنیاد ویکی مدیا
employee: 400
industry: education
type: nonprofit
business: donation
revenue: 100
marketCap: na
links:
  website: https://wikimediafoundation.org/
  wikipedia: https://en.wikipedia.org/wiki/Wikimedia_Foundation
---
