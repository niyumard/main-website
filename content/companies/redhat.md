---
title: ردهت
employee: 13000
industry: software
type: subsidiary
business: support
marketCap: 34000
revenue: 3000
links:
  website: https://www.redhat.com/
  wikipedia: https://en.wikipedia.org/wiki/Redhat
---

هسته لینوکس در سال ۱۹۹۱ به وجود آمد. موسسین ردهت که پیشرفت برق آسای آن را دیدند از
آن استفاده کردند و یک توزیع از آن را به وجود آوردند به نام ردهت، و در سال ۱۹۹۳ یک
شرکت به همین نام تاسیس کردند. به لطف رشد بسیار زیاد لینوکس و ترند شدن آن، ردهت
پیشرفت عظیمی کرد.

بیزینس پلن ردهت در آن زمان، پشتیبانی بود؛ که یکی از رایج ترین روش های کسب درآمد
از داده های عمومی است. تقریبا تمام شرکت های فعال لینوکس، این روش را به عنوان یکی
از اصلی ترین روش های کسب درآمد خود دنبال می کنند و ردهت به عنوان نماینده آن ها
در این لیست حاضر شده است. البته این مدل فقط مخصوص شرکت های لینوکسی و حتی فقط مخصوص
نرم افزار نیست، و هر شرکت تولید کننده داده عمومی که استفاده و بهبود آن نیاز به
تخصص فنی دارد می تواند آن را در پیش بگیرد.

اما پشتیبانی یعنی چه؟ هنگامی که شما مجوز استفاده از یک داده انحصاری را خریداری
می کنید، معمولا به طور ضمنی یک چیز دیگر نیز خریداری می کنید؛ این که این محصول
واقعا کار می کند! یعنی اگر آن محصول کار خواسته شده را انجام ندهد و قابلیت های
خواسته شده را نداشته باشد، می توانید به نهاد ریگولاتوری شکایت کنید و از فروشنده
درخواست غرامت کنید. چنین چیزی در داده های عمومی وجود ندارد و داده های عمومی اصطلاحا
بدون ضمانت هستند. یعنی شما ممکن است داده را دانلود و محصول را بسازید اما با شرایط
یا دستگاه شما کار نکند یا قابلیتی که شما نیاز دارید را نداشته باشد. در این جا
اگر شما توانایی فنی لازم را داشته باشید می توانید خودتان آن مشکل را رفع کنید اما
در پروژه های بزرگ و پیچیده ای مانند سیستم عامل، این از عهده بسیاری از افراد و
شرکت ها خارج است. پس شما می توانید این گارانتی را به صورت یک محصول از شرکت هایی
مانند ردهت بخرید و از آن به بعد در صورت بروز مشکل، از ردهت شکایت کنید تا مشکلتان
را حل کند.

توجه کنید که انحصار پشتیبانی داده های عمومی در اختیار یک شرکت نیست و هر شرکتی
می تواند این خدمت را ارائه دهد. اما شرکت سازنده و مشارکت کننده به علت مهارت
بیشتر و صاحب بودن علامت تجاری و علل مشابه، مزیت رقابتی دارد. شما هنگامی که ردهت
را در اینترنت جستجو می کنید یا آن را از سایت آن دانلود می کنید، پشتیبانی به شما
پیشنهاد داده می شود و می توانید آن را خریداری کنید. شما ردهت که هم نام سیستم عامل
و یک شرکت چند ده میلیارد دلاری با مخازن عظیم برنامه تست شده توسط هزاران متخصص است را
برای پشتیبانی ترجیح می دهید یا شرکت اصغر آقا با قیمت کمتر را؟ احتمالا ردهت بیشتر
انتخاب شده است که به این درآمد رسیده است. اگر چه شرکت های متفرقه نیز خدمات پشتیبانی
انجام می دهند. به شرکت های پشتیبانی که در پروژه های بالادست مشارکت نمی کنند، شرکت های
انگل گفته می شود.
